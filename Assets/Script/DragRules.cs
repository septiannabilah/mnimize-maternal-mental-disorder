﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragRules : MonoBehaviour
{
    public static List<bool> checking = new List<bool>();
    public GameObject[] items;

    public GameObject startPanel;
    public GameObject congratsPanel;

    // Start is called before the first frame update
    void Start()
    {
        checking = new List<bool>();
        congratsPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckPosition())
        {
            //Debug.Log("You win");
            congratsPanel.SetActive(true);
            Time.timeScale = 0;
        }
    }

    bool CheckPosition()
    {
        if (items.Length == checking.Count)
        {
            return true;
        }
        return false;
    }

    GameObject PanelTrue()
    {
        
        return null;
    }
}
