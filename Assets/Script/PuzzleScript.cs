﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleScript : MonoBehaviour
{
    private List<bool> holderList = new List<bool>();
    public Transform[] holders;
    public Transform[] pieces;

    public GameObject startPanel;
    public GameObject congratsPanel;

    private bool[] check = new bool[9];
    private bool win;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < holders.Length; i++)
        {
            holderList.Add(false);
            check[i] = false;
        }
        win = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckPosition();

        win = CheckBoolArrays();

        if (win)
        {
            Debug.Log("yay winning");
            congratsPanel.SetActive(true);
            Time.timeScale = 0;
        }
    }

    void CheckPosition()
    {
        for (int i = 0; i < pieces.Length; i++)
        {
            if (pieces[i].position == holders[i].position)
                //holderList[i] = true;
                check[i] = true;
        }
    }

    bool CheckBoolArrays()
    {
        for (int i = 0; i < check.Length; i++)
        {
            if (check[i] == false)
                return false;
        }
        return true;
    }
}
