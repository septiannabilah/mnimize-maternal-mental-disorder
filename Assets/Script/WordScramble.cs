﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[System.Serializable]
public class Word
{
    public string word;
    [Header("leave empty if you want to randomized")]
    public string desiredRandom;

    public string GetString ()
    {
        if (!string.IsNullOrEmpty(desiredRandom))
        {
            return desiredRandom;
        }

        string result = word;
        

        while (result == word)
        {
            result = "";
            List<char> characters = new List<char>(word.ToCharArray());
            while (characters.Count > 0)
            {
                int indexChar = Random.Range(0, characters.Count - 1);
                result += characters[indexChar];

                characters.RemoveAt(indexChar);
            }
        }
        return result;
    }
}

public class WordScramble : MonoBehaviour
{
    public Word[] words;
    public ImageController imageController;

    public GameObject Endpanel;

    [Header("UI REFERENCE")]
    public CharObject prefab;
    public Transform container;
    public float space;
    public float lerpSpeed = 5f;
    public float waitChecking = 2f;

    List<CharObject> charObjects = new List<CharObject>();
    CharObject firstSelected;

    public int currentWord;
    public static WordScramble main;

    private void Awake()
    {
        main = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        imageController = FindObjectOfType<ImageController>();
        ShowScramble(currentWord);

        ImageFill();
    }

    // Update is called once per frame
    void Update()
    {
        ImageFill();
        RepositionObject();
    }

    // re-positioning the alphabet
    void RepositionObject()
    {
        if (charObjects.Count == 0)
        {
            return;
        }

        //float center = ((charObjects.Count - 1) / 2);
        //for (int i = 0; i < charObjects.Count; i++)
        //{
        //    if (charObjects.Count % 2 == 0)
        //    {
        //        center = (charObjects.Count / 2);
        //        if (i + 1 == center)
        //        {
        //            charObjects[i].rectTransform.anchoredPosition =
        //               Vector2.Lerp(charObjects[i].rectTransform.anchoredPosition,
        //               new Vector2(space / -2, 0), lerpSpeed * Time.deltaTime);
        //        }
        //        else if (i == center)
        //        {
        //            charObjects[i].rectTransform.anchoredPosition =
        //                Vector2.Lerp(charObjects[i].rectTransform.anchoredPosition,
        //                new Vector2(space / 2, 0), lerpSpeed * Time.deltaTime);
        //        }
        //        else
        //        {
        //            charObjects[i].rectTransform.anchoredPosition =
        //               Vector2.Lerp(charObjects[i].rectTransform.anchoredPosition,
        //               new Vector2(((i - center) * space) + space / 2, 0), lerpSpeed * Time.deltaTime);
        //        }
        //    }
        //    else
        //    {
        //        charObjects[i].rectTransform.anchoredPosition =
        //            Vector2.Lerp(charObjects[i].rectTransform.anchoredPosition,
        //            new Vector2((i - center) * space, 0), lerpSpeed * Time.deltaTime);
        //    }
            
        //    charObjects[i].index = i;
        //}

        for (int i = 0; i < charObjects.Count; i++)
        {
           
            switch (i)
            {
                case 0:
                    charObjects[i].rectTransform.anchoredPosition =
                       Vector2.Lerp(charObjects[i].rectTransform.anchoredPosition,
                       new Vector2(-260, 470), lerpSpeed * Time.deltaTime);
                    //ImageFill(i);
                    break;
                case 1:
                    charObjects[i].rectTransform.anchoredPosition =
                       Vector2.Lerp(charObjects[i].rectTransform.anchoredPosition,
                       new Vector2(260, 470), lerpSpeed * Time.deltaTime);
                    //ImageFill(i);
                    break;
                case 2:
                    charObjects[i].rectTransform.anchoredPosition =
                       Vector2.Lerp(charObjects[i].rectTransform.anchoredPosition,
                       new Vector2(-260, -393), lerpSpeed * Time.deltaTime);
                    //ImageFill(i);
                    break;
                case 3:
                    charObjects[i].rectTransform.anchoredPosition =
                       Vector2.Lerp(charObjects[i].rectTransform.anchoredPosition,
                       new Vector2(280, -393), lerpSpeed * Time.deltaTime);
                    //ImageFill(i);
                    break;
                default:
                    break;
            }

            charObjects[i].index = i;
        }

    }

    // show random word to the screen
    public void ShowScramble()
    {
        ShowScramble(Random.Range(0, words.Length - 1)); 
    }

    // show word from collection with certain index
    public void ShowScramble (int index)
    {
        charObjects.Clear();
        foreach(Transform child in container)
        {
            Destroy(child.gameObject);
        }

        if (index > words.Length - 1)
        {
            Debug.LogError("Index out of range, please enter range between 0 - " +
                (words.Length - 1).ToString());
            return;
        }

        char[] chars = words[index].GetString().ToCharArray();
        foreach (char c in chars)
        {
            CharObject clone = Instantiate(prefab.gameObject).GetComponent<CharObject>();
            clone.transform.SetParent(container);
            clone.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            clone.text.GetComponent<Text>().text = "";

            charObjects.Add(clone.Init(c));
        }

        // reSizing alphabet so it could fit in container
        float width = 0;
        float height = 0;
        if (charObjects.Count >= 10)
        {
            for (int i = 0; i < charObjects.Count; i++)
            {
                width = charObjects[i].rectTransform.sizeDelta.x - 10;
                height = charObjects[i].rectTransform.sizeDelta.y - 10;
                space = width;
                Debug.Log("width = " + width);
                charObjects[i].rectTransform.sizeDelta = new Vector2(width, height);
            }
        } 
        else if (charObjects.Count >= 5)
        {
            for (int i = 0; i < charObjects.Count; i++)
            {
                width = charObjects[i].rectTransform.sizeDelta.x - 5;
                height = charObjects[i].rectTransform.sizeDelta.y - 5;
                space = width;
                Debug.Log("width = " + width);
                charObjects[i].rectTransform.sizeDelta = new Vector2(width, height);
            }
        }

        currentWord = index;
    }

    public void Swap (int indexA, int indexB)
    {
        CharObject tempA = charObjects[indexA];

        charObjects[indexA] = charObjects[indexB];
        charObjects[indexB] = tempA;

        //Debug.Log("charObjects tempA : " + tempA.text.text);
        //Debug.Log("charObjects[indexA] : " + charObjects[indexA].text.text);
        //Debug.Log("charObjects[indexB] : " + charObjects[indexB].text.text);

        charObjects[indexA].transform.SetAsLastSibling();
        charObjects[indexB].transform.SetAsLastSibling();

        CheckWord();
    }

    public void Select (CharObject charObject)
    {
        if (firstSelected)
        {
            Swap(firstSelected.index, charObject.index);
            //Debug.Log("firstSelected1 : " + firstSelected.text.text);
            //Debug.Log("charObject : " + charObject.text.text);

            //unselect
            firstSelected.Select();
            charObject.Select();

        } else
        {
            firstSelected = charObject;
            //charObject = null;
            //Debug.Log("firstSelected : " + firstSelected.text.text);
            
        }
    }

    public void UnSelect()
    {
        firstSelected = null;
    }

    public void CheckWord()
    {
        StartCoroutine(CoCheckWord()); 
    }

    IEnumerator CoCheckWord()
    {
        yield return new WaitForSeconds(waitChecking);

        string word = "";
        foreach (CharObject charObject in charObjects)
        {
            word += charObject.character;
        }

        if (word == words[currentWord].word)
        {
            if (currentWord + 1 < words.Length)
            {
                currentWord++;
                ShowScramble(currentWord);
            }
            else
            {
                yield return new WaitForSeconds(waitChecking);
                Endpanel.SetActive(true);
                Time.timeScale = 0;
            }
            
        }
    }

    public void ImageFill()
    {
        
        foreach(CharObject charObject in charObjects)
        {
            charObject.image.sprite = Resources.Load<Sprite>("ImageRound/" + "Round" + (currentWord+1) + "_" + charObject.character);
        }
        
    }
}
