﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    public Puzzle puzzlePrefab;
    private List<Puzzle> puzzleList = new List<Puzzle>();
    private List<Vector3> puzzlePosition = new List<Vector3>();
    private List<Vector3> winPosition = new List<Vector3>();
    private List<int> RandomNumber = new List<int>();
    private List<bool> puzzleCheck = new List<bool>();

    private bool checkWin;

    private Vector2 startPosition = new Vector2(-4.0f, 1.91f);
    private Vector2 offset = new Vector2(2.02f, 1.52f);

    // moving puzzles
    public LayerMask collisionMask;

    // collision
    Ray ray_up, ray_down, ray_left, ray_right;
    RaycastHit hit;
    private BoxCollider coll;
    private Vector2 collider_size;
    private Vector2 collider_center;

    public string folderName;
    public GameObject Fullpict;

    // Start is called before the first frame update
    void Start()
    {
        checkWin = false;
        SpawnPuzzle(14);
        SetStartPosition();
        MixPuzzle();
        ApplyMaterial();
    }

    // Update is called once per frame
    void Update()
    {
        MovePuzzle();
        //CheckWin();

        if (puzzleCheck.Contains(true))
            Debug.Log("You Win this");
    }

    void SpawnPuzzle(int number)
    {
        for (int i = 0; i <= number; i++)
        {
            puzzleList.Add(Instantiate(puzzlePrefab, new Vector3(0.0f, 0.0f, 0.0f), new Quaternion(0.0f, 0.0f, 180.0f, 0.0f)) as Puzzle);
        }
    }

    void SetStartPosition()
    {
        // first line
        puzzleList[0].transform.position = new Vector2(startPosition.x, startPosition.y);
        puzzleList[1].transform.position = new Vector2(startPosition.x + offset.x, startPosition.y);
        puzzleList[2].transform.position = new Vector2(startPosition.x + 2 * offset.x, startPosition.y);

        // second line 
        puzzleList[3].transform.position = new Vector2(startPosition.x, startPosition.y - offset.y);
        puzzleList[4].transform.position = new Vector2(startPosition.x + offset.x, startPosition.y - offset.y);
        puzzleList[5].transform.position = new Vector2(startPosition.x + 2 * offset.x, startPosition.y - offset.y);
        puzzleList[6].transform.position = new Vector2(startPosition.x + 3 * offset.x, startPosition.y - offset.y);

        // third line
        puzzleList[7].transform.position = new Vector2(startPosition.x, startPosition.y - 2 * offset.y);
        puzzleList[8].transform.position = new Vector2(startPosition.x + offset.x, startPosition.y - 2 * offset.y);
        puzzleList[9].transform.position = new Vector2(startPosition.x + 2 * offset.x, startPosition.y - 2 * offset.y);
        puzzleList[10].transform.position = new Vector2(startPosition.x + 3 * offset.x, startPosition.y - 2 * offset.y);

        // fourth line 
        puzzleList[11].transform.position = new Vector2(startPosition.x, startPosition.y - 3 * offset.y);
        puzzleList[12].transform.position = new Vector2(startPosition.x + offset.x, startPosition.y - 3 * offset.y);
        puzzleList[13].transform.position = new Vector2(startPosition.x + 2 * offset.x, startPosition.y - 3 * offset.y);
        puzzleList[14].transform.position = new Vector2(startPosition.x + 3 * offset.x, startPosition.y - 3 * offset.y);
    }

    void MovePuzzle()
    {
        foreach(Puzzle puzzle in puzzleList)
        {
            puzzle.move_Amount = offset;

            if (puzzle.clicked)
            {
                // ray up and down
                coll = puzzle.GetComponent<BoxCollider>();
                collider_size = coll.size;
                collider_center = coll.center;

                float move_amount = offset.x;
                float direction = Mathf.Sign(move_amount);

                // set rays up and down
                float x = (puzzle.transform.position.x + collider_center.x - collider_size.x / 2) + collider_size.x / 2;
                float y_up = puzzle.transform.position.y + collider_center.y + collider_size.y / 2 * direction;
                float y_down = puzzle.transform.position.y + collider_center.y + collider_size.y / 2 * -direction;

                ray_up = new Ray(new Vector2(x, y_up), new Vector2(0f, direction));
                ray_down = new Ray(new Vector2(x, y_down), new Vector2(0f, -direction));

                // draw rays
                Debug.DrawRay(ray_up.origin, ray_up.direction);
                Debug.DrawRay(ray_down.origin, ray_down.direction);

                // set rays left and right
                float y = (puzzle.transform.position.y + collider_center.y - collider_size.y / 2) + collider_size.y / 2;
                float x_right = puzzle.transform.position.x + collider_center.x + collider_size.x / 2 * direction;
                float x_left = puzzle.transform.position.x + collider_center.x + collider_size.x / 2 * -direction;

                ray_left = new Ray(new Vector2(x_left, y), new Vector2(-direction, 0f));
                ray_right = new Ray(new Vector2(x_right, y), new Vector2(direction, 0f));

                // draw rays
                Debug.DrawRay(ray_left.origin, ray_left.direction);
                Debug.DrawRay(ray_right.origin, ray_right.direction);

                // check collision
                if ((Physics.Raycast(ray_up, out hit, 1.0f, collisionMask) == false) && (puzzle.moved == false) && 
                    (puzzle.transform.position.y < startPosition.y))
                {
                    puzzle.go_Up = true;
                    Debug.Log("Move up Allowed, Moved = " + puzzle.moved);
                }
                else if ((Physics.Raycast(ray_down, out hit, 1.0f, collisionMask) == false) && (puzzle.moved == false) &&
                    (puzzle.transform.position.y >= (startPosition.y - 2.1f * offset.y)))
                {
                    puzzle.go_Down = true;
                    Debug.Log("Move Down Allowed, moved = " + puzzle.moved);
                }
                else if ((Physics.Raycast(ray_left, out hit, 1.0f, collisionMask) == false) && (puzzle.moved == false) &&
                    (puzzle.transform.position.x > startPosition.x))
                {
                    puzzle.go_Left = true;
                    Debug.Log("Move Left Allowed, moved = " + puzzle.moved);
                }
                else if ((Physics.Raycast(ray_right, out hit, 1.0f, collisionMask) == false) && (puzzle.moved == false) &&
                    (puzzle.transform.position.x < (startPosition.x + 3 * offset.x)))
                {
                    puzzle.go_Right = true;
                    Debug.Log("Move Right Allowed, moved = " + puzzle.moved);
                }

            }
        }
    }

    void ApplyMaterial()
    {
        string filePath;
        for (int i = 1; i <= puzzleList.Count; i++)
        {
            Texture2D texture;
            if (i <= 3)
                texture = Resources.Load<Texture2D>("Puzzle/" + "puzzleFull_" + i);
            else
                texture = Resources.Load<Texture2D>("Puzzle/" + "puzzleFull_" + (i+1));

            //Load a Texture (Assets/Resources/Textures/texture01.png)
            //var texture = Resources.Load<Texture2D>("Puzzle/" + "puzzle-pict_" + i);
            puzzleList[i - 1].GetComponent<Renderer>().material.SetTexture("_MainTex", texture);

        }

        filePath = "Asset/" + "Puzzle" + "/puzzleFull";
        Texture2D mat1 = Resources.Load(filePath, typeof(Texture2D)) as Texture2D;
        var texture1 = Resources.Load<Texture2D>("Puzzle/puzzleFull");
        Fullpict.GetComponent<Renderer>().material.SetTexture("_MainTex", texture1);
    }

    void MixPuzzle()
    {
        int number;

        foreach (Puzzle p in puzzleList)
        {
            puzzlePosition.Add(p.transform.position);
            winPosition = puzzlePosition;
        }

        foreach (Puzzle p in puzzleList)
        {
            number = Random.Range(0, puzzleList.Count);

            while (RandomNumber.Contains(number))
            {
                number = Random.Range(0, puzzleList.Count);
            }
            RandomNumber.Add(number);
            p.transform.position = puzzlePosition[number];
        }
    }

    void CheckWin()
    {

        int i = 1;
        foreach (Puzzle p in puzzleList)
        {
            
            if(p.transform.position == puzzlePosition[1])
            {
                puzzleCheck[i] = true;
            }
            else
            {
                break;
            }
        }
    }
}
