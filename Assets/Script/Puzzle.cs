﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Position
{
    float posX;
    float posY;
    float posZ;
}

public class Puzzle : MonoBehaviour
{
    [HideInInspector]
    public bool clicked = false;

    [HideInInspector]
    public bool go_Left;
    [HideInInspector]
    public bool go_Right;
    [HideInInspector]
    public bool go_Up;
    [HideInInspector]
    public bool go_Down;

    public Vector2 move_Amount; // how much to moves
    public bool moved = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MovePuzzle();
    }

    private void OnMouseDown()
    {
        clicked = true;
    }

    private void OnMouseUp()
    {
        clicked = false;
        moved = false;
    }

    void MovePuzzle()
    {
        if (go_Left)
        {
            transform.position = new Vector2(transform.position.x - move_Amount.x, transform.position.y);
            go_Left = false;
            moved = true;
        }
        if (go_Right)
        {
            transform.position = new Vector2(transform.position.x + move_Amount.x, transform.position.y);
            go_Right = false;
            moved = true;
        }
        if (go_Up)
        {
            transform.position = new Vector2(transform.position.x, transform.position.y + move_Amount.y);
            go_Up= false;
            moved = true;
        }
        if (go_Down)
        {
            transform.position = new Vector2(transform.position.x, transform.position.y - move_Amount.y);
            go_Down = false;
            moved = true;
        }
    }
}
