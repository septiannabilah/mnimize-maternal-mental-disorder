﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonMenu : MonoBehaviour
{
    public GameObject screenToTap;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TouchAnywhere()
    {
        screenToTap.SetActive(false);
        Time.timeScale = 1;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void MainMenu1()
    {
        SceneManager.LoadScene("MainMenu 1");
    }

    public void PosThinkQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void BeautifullPregnancy()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void BeautifullPregnancy1()
    {
        SceneManager.LoadScene("Quiz 2");
    }

    public void BeautifullPregnancy2()
    {
        SceneManager.LoadScene("Quiz 3");
    }

    public void DareToFight1()
    {
        SceneManager.LoadScene("Quiz 1");
    }

    public void DareToFight2()
    {
        SceneManager.LoadScene("WordPuzzle");
    }

    public void PolaMakan()
    {
        SceneManager.LoadScene("DragnDrop1");
    }

    public void PolaMakan2()
    {
        SceneManager.LoadScene("DragnDrop2");
    }

    public void PerawatanDiri()
    {
        SceneManager.LoadScene("DragnDrop3");
    }

    public void PerawatanDiri2()
    {
        SceneManager.LoadScene("DragnDrop4");
    }

    public void OlahragaSenam()
    {
        SceneManager.LoadScene("DragnDrop5");
    }

    public void OlahragaSenam2()
    {
        SceneManager.LoadScene("Puzzle");
    }



}
