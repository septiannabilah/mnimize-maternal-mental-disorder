﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class DataController : MonoBehaviour
{
    public RoundData[] allRoundData;
    public static int thisRound;

    // Use this for initialization
    void Start()
    {
        //DontDestroyOnLoad(gameObject);

        //SceneManager.LoadScene("MenuScreen");

        thisRound = 0;
    }

    public RoundData GetCurrentRoundData()
    {
        return allRoundData[thisRound];
    }

    // Update is called once per frame
    void Update()
    {

    }
}