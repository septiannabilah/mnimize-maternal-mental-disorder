﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

public class QuizController1 : MonoBehaviour
{
    public Text questionDisplayText;
    public Text scoreDisplayText;
    public Text timeRemainingDisplayText;
    public SimpleObjectPool answerButtonObjectPool;
    public Transform answerButtonParent;
    public GameObject questionDisplay;
    public GameObject roundEndDisplay;

    public GameObject correctPanel;
    public GameObject wrongPanel;
    public GameObject congratsPanel;
    public GameObject congrats2Panel;
    public Image imageLevel;
    public GameObject endPanel;

    private DataController dataController;
    private RoundData currentRoundData;
    private QuestionData[] questionPool;

    private bool isRoundActive;
    private float timeRemaining;
    private int questionIndex;
    private int playerScore;
    private List<GameObject> answerButtonGameObjects = new List<GameObject>();

    private List<int> questionAnswered = new List<int>();

    // Use this for initialization
    void Start()
    {
        dataController = FindObjectOfType<DataController>();
        currentRoundData = dataController.GetCurrentRoundData();
        questionPool = currentRoundData.questions;
        timeRemaining = currentRoundData.timeLimitInSeconds;
        UpdateTimeRemainingDisplay();

        playerScore = 0;
        //questionIndex = Random.Range(0, questionPool.Length);

        ShowQuestion();
        isRoundActive = true;

        congratsPanel.SetActive(false);
        imageLevel = gameObject.GetComponent<Image>();

        Time.timeScale = 0;
    }

    private void ShowQuestion()
    {
        RemoveAnswerButtons();
        questionIndex = Random.Range(0, questionPool.Length);

        if (questionAnswered.Contains(questionIndex))
        {
            while (questionAnswered.Contains(questionIndex))
            {
                questionIndex = Random.Range(0, questionPool.Length);
            }
        }

        //questionAnswered.Add(questionIndex);
        QuestionData questionData = questionPool[questionIndex];
        questionDisplayText.text = questionData.questionText;

        if (questionData.questionText.Length > 150)
        {
            questionDisplayText.fontSize = 75 - 30;
        } else if (questionData.questionText.Length < 150 && questionData.questionText.Length > 100)
        {
            questionDisplayText.fontSize = 75 - 15;
        } else if (questionData.questionText.Length < 150)
        {
            questionDisplayText.fontSize = 75;
        }
        

        for (int i = 0; i < questionData.answers.Length; i++)
        {
            GameObject answerButtonGameObject = answerButtonObjectPool.GetObject();
            answerButtonGameObjects.Add(answerButtonGameObject);
            answerButtonGameObject.transform.SetParent(answerButtonParent);

            AnswerButton answerButton = answerButtonGameObject.GetComponent<AnswerButton>();
            answerButton.Setup(questionData.answers[i]);
        }
    }

    private void RemoveAnswerButtons()
    {
        while (answerButtonGameObjects.Count > 0)
        {
            answerButtonObjectPool.ReturnObject(answerButtonGameObjects[0]);
            answerButtonGameObjects.RemoveAt(0);
        }
    }

    public void AnswerButtonClicked(bool isCorrect)
    {
        if (isCorrect)
        {
            playerScore += currentRoundData.pointsAddedForCorrectAnswer;
            scoreDisplayText.text = "Score: " + playerScore.ToString();

            questionAnswered.Add(questionIndex);
            timeRemaining = currentRoundData.timeLimitInSeconds;

            if (playerScore >= 35)
            {
                endPanel.SetActive(true);
                Time.timeScale = 0;
            } else
            {
                if (questionPool.Length >= questionIndex + 1)
                {
                    //questionIndex++;
                    correctPanel.SetActive(true);
                    Time.timeScale = 0;
                    ShowQuestion();
                }
                else
                {
                    endPanel.SetActive(true);
                    Time.timeScale = 0;
                }
            } 
        } else
        {
            Debug.Log("Wrong Answer");
            playerScore -= 1;
            scoreDisplayText.text = "Score: " + playerScore.ToString();

            wrongPanel.SetActive(true);
            Time.timeScale = 0;

            ShowQuestion();
            timeRemaining = currentRoundData.timeLimitInSeconds;
        }

    }

    public void EndRound()
    {
        isRoundActive = false;

        questionDisplay.SetActive(false);
        roundEndDisplay.SetActive(true);
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("MenuScreen");
    }

    private void UpdateTimeRemainingDisplay()
    {
        timeRemainingDisplayText.text = "Time: " + Mathf.Round(timeRemaining).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (isRoundActive)
        {
            timeRemaining -= Time.deltaTime;
            UpdateTimeRemainingDisplay();

            if (timeRemaining <= 0f)
            {
                //EndRound();
                ShowQuestion();
                timeRemaining = currentRoundData.timeLimitInSeconds;
            }

        }
    }
}
