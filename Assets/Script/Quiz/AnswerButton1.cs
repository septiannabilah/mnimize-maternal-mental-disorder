﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class AnswerButton1 : MonoBehaviour
{

    public Text answerText;

    private AnswerData answerData;
    private QuizController1 quizController1;

    // Use this for initialization
    void Start()
    {
        quizController1 = FindObjectOfType<QuizController1>();
    }

    public void Setup(AnswerData data)
    {
        answerData = data;
        answerText.text = answerData.answerText;
    }


    public void HandleClick()
    {
        quizController1.AnswerButtonClicked(answerData.isCorrect);
    }
}