﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class AnswerButton : MonoBehaviour
{

    public Text answerText;

    private AnswerData answerData;
    private QuizController gameController;

    // Use this for initialization
    void Start()
    {
        gameController = FindObjectOfType<QuizController>();
    }

    public void Setup(AnswerData data)
    {
        answerData = data;
        answerText.text = answerData.answerText;
    }


    public void HandleClick()
    {
        gameController.AnswerButtonClicked(answerData.isCorrect);
    }
}