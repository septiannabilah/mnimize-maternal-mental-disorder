﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObject : MonoBehaviour
{
    private Vector2 tempPosition;
    public GameObject placeHolder;
    private static bool locked;

    private Vector3 screenPoint;
    private Vector3 offset;

    private DragRules dragRules;

    private void Start()
    {
        tempPosition = transform.position;
        locked = false;
    }

    void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
    }

    void OnMouseDrag()
    {
        Vector2 cursorPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
        transform.position = cursorPosition;
    }

    void OnMouseUp()
    {
        if (Mathf.Abs(transform.position.x - placeHolder.transform.position.x) <= 0.5f &&
                        Mathf.Abs(transform.position.y - placeHolder.transform.position.y) <= 0.5f)
        {
            //transform.position = new Vector2(placeHolder.position.x, placeHolder.position.y);
            //locked = true;

            placeHolder.GetComponent<SpriteRenderer>().sortingOrder = 2;
            transform.position = new Vector2(placeHolder.transform.position.x, placeHolder.transform.position.y);
            this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 0;
            //dragRules.checking.Add(true);
            DragRules.checking.Add(true);
        }
        else
        {
            transform.position = Vector2.Lerp(tempPosition, placeHolder.transform.position, 1.0f * Time.deltaTime);
            //transform.position = tempPosition;
        }
        
    }
}
