﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageController : MonoBehaviour
{
    public ImageRound[] imageRounds;

    public ImageRound GetImage(int imageIndex)
    {
        return imageRounds[imageIndex];
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
