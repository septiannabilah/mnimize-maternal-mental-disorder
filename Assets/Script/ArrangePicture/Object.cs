﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Object : MonoBehaviour
{
    public char character;
    public Text text;
    public Image image;
    public RectTransform rectTransform;
    public int index;

    [Header("Appearence")]
    public Color normalColor;
    public Color selectedColor;

    bool isSelected = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Select()
    {
        isSelected = !isSelected;
        image.color = isSelected ? selectedColor : normalColor;

        if (isSelected)
        {
            //WordScramble.main.Select(this);
            Debug.Log("Select this Button : " + isSelected);
            //image.color = selectedColor;
        }
        else if (!isSelected)
        {
            //WordScramble.main.UnSelect();
            Debug.Log("Unselect this Button : " + isSelected);
            //image.color = normalColor;
        }
    }

}
